package com.nuruldwifebriliyanti.skripsi.adapter;

import java.util.ArrayList;
import java.util.List;

import com.nuruldwifebriliyanti.skripsi.R;
import com.nuruldwifebriliyanti.skripsi.model.Anatomi;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<Anatomi> {
	private Context mcontext;
	private List<Anatomi> dataset;
	

	public ListAdapter(Context mcontext, List<Anatomi> data) {
		super(mcontext, R.layout.item_list, data);
		this.mcontext = mcontext;
		this.dataset = data;
	}

	public View getView(final int position, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		Anatomi anatomi = getItem(position);
		arg1 = View.inflate(mcontext, R.layout.item_list, null);

		TextView txtNama = (TextView) arg1.findViewById(R.id.text_list);
		txtNama.setText(anatomi.getNama());
		return arg1;
	}
	
	
}
