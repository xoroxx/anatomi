package com.nuruldwifebriliyanti.skripsi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.TextView;

public class ScoreActivity extends AppCompatActivity implements OnClickListener{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.score_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		Intent intent = getIntent();
		// mengambil nilai / score 
        int score = intent.getIntExtra("score", 0);
		
		TextView nilai = (TextView)findViewById(R.id.textNilai);
		TextView high = (TextView)findViewById(R.id.textHigh);
		nilai.setText(String.valueOf(score));
		// menyimpan highscroce ke cache
		SharedPreferences mypref = getPreferences(MODE_PRIVATE);
        int highscore = mypref.getInt("highscore",0);
        if(highscore>= score)
            high.setText(String.valueOf(highscore));
        else
        {
            high.setText("New Best : "+score);
            SharedPreferences.Editor editor = mypref.edit();
            editor.putInt("highscore", score);
            editor.commit();
        }
        
        TextView keluar = (TextView)findViewById(R.id.textExit);
        TextView ulang = (TextView)findViewById(R.id.textUlang);
        
        keluar.setOnClickListener(this);
        ulang.setOnClickListener(this);
        
        MediaPlayer mp = new MediaPlayer();
        if (!mp.isPlaying()) {
			mp = MediaPlayer.create(this, R.raw.applause);
			mp.start();	
		}else {
			mp.stop();
		}
        
	}
	@Override
	public void onClick(View v){
		int id = v.getId();
		if (id == R.id.textExit){
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		} else if (id == R.id.textUlang){

			Intent intent = new Intent(this, KuisActivity.class);
			startActivity(intent);
		}
	}
	
	@Override
	public void onBackPressed()
	{
		finish();
		return;
	}

}
