package com.nuruldwifebriliyanti.skripsi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nuruldwifebriliyanti.skripsi.database.DataHelper;
import com.nuruldwifebriliyanti.skripsi.model.Soal;

public class KuisActivity extends AppCompatActivity {
	private DataHelper datahelper;
	private TextView txtno, txtwaktu, txtsoal;
	private ImageView img;
	private List<Soal> listSoal;
	private CountDownTimer mCountDownTimer;
	int urutanPertanyaan = 0;

	private final long startTime = 30 * 1000;
	private final long interval = 1 * 1000;
	private int mScore = 0;
	private String mAnswer;
	Button btnA, btnB, btnC;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.kuis_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		datahelper = new DataHelper(this);
		listSoal = new ArrayList<Soal>();
		listSoal = datahelper.getSoal();

		txtno = (TextView) findViewById(R.id.textViewHalaman);
		txtwaktu = (TextView) findViewById(R.id.textViewWaktu);
		txtsoal = (TextView) findViewById(R.id.textViewSoal);
		img = (ImageView) findViewById(R.id.gambarKuis);

		btnA = (Button) findViewById(R.id.btnA);
		btnB = (Button) findViewById(R.id.btnB);
		btnC = (Button) findViewById(R.id.btnC);

		mulaiKuis();

	}

	protected void mulaiKuis() {
		mCountDownTimer = new Timer(startTime, interval);
		tampilSoal(urutanPertanyaan);
	}

	private void tampilSoal(int urutan) {
		
		if (urutan < listSoal.size()) {
			mCountDownTimer.start();
			noUrut(); //tampilkan nomer urut soal
			// tampilkan soal
			try {
				Soal soal = listSoal.get(urutan);
				txtsoal.setText(soal.getSoal());
				mAnswer = soal.getJwban();
				btnA.setText(soal.getPil_a());
				btnB.setText(soal.getPil_b());
				btnC.setText(soal.getPil_c());
				int image = getApplicationContext().getResources()
						.getIdentifier(soal.getGambar(), "drawable",
								getApplicationContext().getPackageName());
				img.setImageDrawable(getResources().getDrawable(image));
				urutanPertanyaan++;
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			mCountDownTimer.cancel();
			showScore();
		}

	}

	// class timer
	class Timer extends CountDownTimer {

		public Timer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			txtwaktu.setText("Waktu Habis");
			Toast.makeText(KuisActivity.this, "Waktu Habis", Toast.LENGTH_SHORT)
					.show();
			showScore();

		}

		@Override
		public void onTick(long arg0) {
			// TODO Auto-generated method stub
			txtwaktu.setText("" + (int) (arg0 / 1000) + " detik");
		}

	}

	public void onClick(View arg0) {
//		MediaPlayer mp = new MediaPlayer();
		
		mCountDownTimer.cancel();
		Button answer = (Button) arg0;
		if (answer.getText().equals(mAnswer)) {
//			if (!mp.isPlaying()) {
//				mp = MediaPlayer.create(this, R.raw.applause);
//				mp.start();	
//			}else {
//				mp.stop();
//			}
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Yeey Kamu Benar !")
					.setCancelable(false)
					.setPositiveButton("LANJUT",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									
									tampilSoal(urutanPertanyaan);
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
			// nilai ditambah
			updateScore(10);
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Yah Kamu Salah !")
					.setCancelable(false)
					.setPositiveButton("LANJUT",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									tampilSoal(urutanPertanyaan);
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}

	}

	// membuat nilai
	private void updateScore(int point) {
		mScore = mScore + point;
	}

	// tampil nilai
	private void showScore() {
		Intent intent = new Intent(KuisActivity.this, ScoreActivity.class);
		intent.putExtra("score", mScore);
		startActivity(intent);
		finish();
	}

	// buat nomor urut
	private void noUrut() {
		txtno.setText("Soal " + (urutanPertanyaan + 1) + "/" + listSoal.size());
	}
	
	@Override
	public void onBackPressed()
	{
		//mCountDownTimer.cancel();
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
		return;
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		mCountDownTimer.cancel();
		super.onDestroy();
		
	}
	
}
