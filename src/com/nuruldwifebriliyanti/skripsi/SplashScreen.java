package com.nuruldwifebriliyanti.skripsi;


import com.nuruldwifebriliyanti.skripsi.R;


import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends AppCompatActivity{
	 private static int splashInterval = 3000;
	 
	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	 
	        setContentView(R.layout.splash_layout);

	        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	                WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        new Handler().postDelayed(new Runnable() {
	 
	 
	            @Override
	            public void run() {
	                // TODO Auto-generated method stub
	                Intent i = new Intent(getApplicationContext(), MainActivity.class);
	                startActivity(i); 
	            }
	        }, splashInterval);
	 
	    };
}
